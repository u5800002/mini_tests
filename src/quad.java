import java.util.Scanner;

/**
 * Created by guest on 1/03/16.
 */
public class quad {
    public static void main(String[] args) {
        int n = new  Scanner(System.in).nextInt();

        for (int i = 0; i < n/2; i++) {
            for (int k = 0; k < n/2 - i; k++) {
                System.out.print(" ");
            }

            for (int l = 0; l < 2*i + 1; l++) {
                System.out.print("#");
            }

            System.out.println();
        }


        for (int i = n/2; i > -1; i--) {
            for (int k = n/2 - i; k > 0; k--) {
                System.out.print(" ");
            }

            for (int l =  2*i + 1; l > 0; l--) {
                System.out.print("#");
            }

            System.out.println();
        }


    }
}
